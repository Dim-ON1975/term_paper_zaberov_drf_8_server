#usermod -aG docker gitlab-runner
#chown -R gitlab-runner /home/gitlab-runner/builds/

# переходим в директорию с django-проектом
#cd code
## создаём виртуальное окружение
#python3.11 -m venv env
## активируем виртуальное окружение
#source env/bin/activate
## устанавливаем зависимости
#pip install -r requirements.txt
## деактивируем виртуальное окружение
#deactivate
## переходим на уровень выше
#cd ..
# запускаем docker в фоновом режиме
#docker-compose up --build
docker-compose up --build -d
docker-compose exec app python manage.py migrate
docker-compose exec app python manage.py loaddata fixtures/users_data.json
docker-compose exec app pyhton manage.py loaddata fixtures/habits_data.json


